package com.androidappers.flowpath;

import android.os.Bundle;
import android.app.Activity;

public class SampleActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
    }

}
