package com.androidappers.flowpath;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.io.FileDescriptor;
import java.io.IOException;

import States.GameState;
import interfaces.AndroidConnectionInterface;

import static android.content.ContentValues.TAG;

public class AndroidLauncher extends AndroidApplication implements AndroidConnectionInterface {
    private int READ_REQUEST_CODE = 202;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new FlowPath(this), config);
    }

    @Override
    public void disToast(final String s) {
//        handler.post(() ->
//                Toast.makeText(AndroidLauncher.this, s, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void picImageFromFile() {
        performFileSearch();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
    //    MultiDex.install(this);
        super.attachBaseContext(newBase);
    }
    @Override
    public void startNewActivity() {
    }
    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Toast.makeText(this, "Uri: " + uri.toString(), Toast.LENGTH_SHORT).show();
                Log.i("URI:", uri.toString());
                FlowPath.gameStateManager.push(new GameState(FlowPath.gameStateManager,dumpImageMetaData(uri)));
            }
        }
    }
    public String dumpImageMetaData(Uri uri) {

        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        Cursor cursor = this.getContentResolver()
                .query(uri, null, null, null, null, null);

        try {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                Log.i(TAG, "Display Name: " + displayName);

                int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                // If the size is unknown, the value stored is null.  But since an
                // int can't be null in Java, the behavior is implementation-specific,
                // which is just a fancy term for "unpredictable".  So as
                // a rule, check if it's null before assigning to an int.  This will
                // happen often:  The storage API allows for remote files, whose
                // size might not be locally known.
                String size = null;
                if (!cursor.isNull(sizeIndex)) {
                    // Technically the column stores an int, but cursor.getString()
                    // will do the conversion automatically.
                    return cursor.getString(sizeIndex);
                } else {
                    size = "Unknown";
                }
                Log.i("size", "Size: " + size);
            }
        } finally {
            cursor.close();
        }
        return null;
    }
}
