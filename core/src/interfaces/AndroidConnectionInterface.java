package interfaces;

/**
 * Created by madhav on 02-03-2017.
 */

public interface AndroidConnectionInterface {
    public void disToast(String s);

    public void picImageFromFile();

    public void startNewActivity();
}
