package interfaces;

/**
 * Created by Madhav.Gor on 16/02/17.
 */

public interface OnClickListener {
    public void onClick(String id);
}
