package com.androidappers.flowpath;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import States.BackGroundState;
import States.GameStateManager;
import States.GameState;
import States.MenuState;

import States.SplashState;
import interfaces.AndroidConnectionInterface;

public class FlowPath extends ApplicationAdapter {
	public static final int WIDTH = 800;
	public static final int HEIGHT = 480;

	private SpriteBatch spriteBatch;
	public  static GameStateManager gameStateManager;
	public AndroidConnectionInterface androidConnectionInterface;

	public FlowPath(AndroidConnectionInterface androidConnectionInterface) {
		this.androidConnectionInterface = androidConnectionInterface;
	}

	@Override
	public void create() {
		spriteBatch = new SpriteBatch();
		gameStateManager = new GameStateManager(androidConnectionInterface);
		gameStateManager.push(new BackGroundState(gameStateManager));
		gameStateManager.push(new SplashState(gameStateManager));
		Gdx.gl.glClearColor(0, 0, 0, 1);
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gameStateManager.update(Gdx.graphics.getDeltaTime());
		gameStateManager.render(spriteBatch);
	}

	@Override
	public void dispose() {
		super.dispose();
		spriteBatch.dispose();
		gameStateManager.dispose();
	}

}
