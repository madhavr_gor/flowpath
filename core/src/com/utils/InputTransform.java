package com.utils;

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by madhav on 25-03-2017.
 */
public class InputTransform {
    public static float getCursorToModelX() {
        return ((Gdx.input.getX()) * FlowPath.WIDTH) / (Gdx.graphics.getWidth());
    }

    public static float getCursorToModelY() {
        return ((Gdx.graphics.getHeight() - Gdx.input.getY()) * FlowPath.HEIGHT / (Gdx.graphics.getHeight()));
    }

    public static float getModelToCursorX(OrthographicCamera cam, float cursorX) {

        return ((cursorX) * (float) Gdx.graphics.getWidth()) / (cam.viewportWidth);
    }

    public static float getModelToCursorY(OrthographicCamera cam, float cursorY) {
        return (((float) Gdx.graphics.getHeight()) - ((cursorY / cam.viewportHeight) * (float) Gdx.graphics.getHeight()));
    }

    public static  Vector2 getTouchVector() {
        float X = InputTransform.getCursorToModelX();
        float Y = InputTransform.getCursorToModelY();
        return new Vector2(X, Y);
    }

}
