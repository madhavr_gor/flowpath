package model;


import com.badlogic.gdx.utils.Array;

/**
 * Created by Madhav Gor on 22-06-2017.
 */

public class BinEasy {
    private Array<String> file_names=null;

    public Array<String> getFile_names() {
        return file_names;
    }

    public void setFile_names(Array<String> file_names) {
        this.file_names = file_names;
    }
}
