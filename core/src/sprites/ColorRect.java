package sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

import utils.MethodUtils;

/**
 * Created by madhav on 11-04-2017.
 */

public class ColorRect {
    private final float MULTPLIER=0.20f;
    private float width;
    private float height;
    private Vector2 pos;
    private ShapeRenderer shapeRenderer;
    private OrthographicCamera cam;
    private Color color;
    private float alpha;
    private Random random;
    private Vector2 uv;

    public ColorRect(OrthographicCamera cam, float xPos, float yPos, float width, float height, Color color, float alpha) {
        this.cam = cam;
        this.width = width;
        this.height = height;
        this.color = new Color(color.r, color.g, color.b, alpha);
        this.alpha = alpha;
        pos = new Vector2(xPos, yPos);
        uv = new Vector2(MethodUtils.rendomInt(2)==1? (MethodUtils.rendomInt(2)+1)*MULTPLIER:(MethodUtils.rendomInt(2)-3)*MULTPLIER, MethodUtils.rendomInt(2)==1? (MethodUtils.rendomInt(2)+1)*MULTPLIER:MethodUtils.rendomInt(2)-3);
        random=new Random();
        shapeRenderer = new ShapeRenderer();
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Vector2 getPos() {
        return pos;
    }

    public void setPos(Vector2 pos) {
        this.pos = pos;
    }

    public void draw() {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.setProjectionMatrix(cam.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(pos.x, pos.y, width, height);
        shapeRenderer.setColor(color);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void dispose() {
        shapeRenderer.dispose();
    }

    public void update() {
        if (pos.x <  -(getWidth()) || pos.x > cam.viewportWidth) {
            updateVector(true);
        } else if (pos.y < -(getHeight()) || pos.y > cam.viewportHeight) {
            updateVector(false);
        }
        pos.add(uv);
    }

    private void updateVector(boolean isX) {
        if (isX) {
            if (uv.x < 0) {
                uv = new Vector2(Math.abs(uv.x), uv.y);
            } else {
                uv = new Vector2(-uv.x, uv.y);
            }
        } else {
            if (uv.y < 0) {
                uv = new Vector2(uv.x, Math.abs(uv.y));
            } else {
                uv = new Vector2(uv.x,- uv.y);
            }
        }
        color=new Color(MethodUtils.rendomColorFloat(),MethodUtils.rendomColorFloat(),MethodUtils.rendomColorFloat() ,  1);
    }
}
