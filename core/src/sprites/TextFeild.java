package sprites;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import custom.SampleFont;
import interfaces.OnClickListener;

/**
 * Created by Madhav.Gor on 09/03/17.
 */

public class TextFeild {

    private final BitmapFont font;
    private final GlyphLayout layout;
    private final Rectangle boundsText;
    private Vector2 pos;
    private String id;
    private String text;
    private OnClickListener onClickListener;

    public TextFeild(OrthographicCamera cam, float xPoint, float yPoint, String text, OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        this.id = text;
        this.text = text;
        pos = new Vector2(xPoint, yPoint);
        font = SampleFont.getInstance().getBitmapFont();
        layout = new GlyphLayout();
        boundsText = new Rectangle();
        layout.setText(font, text);
        boundsText.set(xPoint, yPoint - layout.height, layout.width, layout.height);
    }

    public void draw(SpriteBatch sb) {
        font.draw(sb, text, pos.x, pos.y);
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }


    public Vector2 getPos() {
        return pos;
    }

    public Rectangle getBoundsText() {
        return boundsText;
    }

    public GlyphLayout getLayout() {
        return layout;
    }

    public BitmapFont getFont() {
        return font;
    }

    public void isClick(Vector2 vector2) {
        if (boundsText.contains(vector2)) {
            if (onClickListener != null) {
                onClickListener.onClick(id);
            }
        }
    }

    public void dispose() {
  //  font.dispose();
    }

}