package sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import interfaces.OnClickListener;

/**
 * Created by madhav on 19-03-2017.
 */

public class ColoredRectangle extends Actor {
private ShapeRenderer shapeRenderer;
    private boolean projectionMatrixSet;
    private float width, height;
    private Vector2 pos;
    private Color color;
    private String id;
    private Rectangle boundsBtn;
    private OnClickListener onClickListener;
    public ColoredRectangle(float x, float y, float width, float height, Color color, OnClickListener onClickListener, String id) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.id=id;
        this.onClickListener = onClickListener;
        shapeRenderer = new ShapeRenderer();
        projectionMatrixSet = false;
        pos = new Vector2(x, y);
        boundsBtn = new Rectangle(pos.x, pos.y, width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
                if (!projectionMatrixSet) {
            shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        }
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(color);
        shapeRenderer.rect(pos.x,pos.y, width, height);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }
    public void isClick(Vector2 vector2) {
        if (boundsBtn.contains(vector2)) {
            if (onClickListener != null) {
                onClickListener.onClick(id);
            }
        }
    }
    public Vector2 getPos() {
        return pos;
    }

    public void setPos(Vector2 pos) {
        this.pos = pos;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }
}
