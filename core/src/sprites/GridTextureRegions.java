package sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

import interfaces.OnRegionClickListner;

/**
 * Created by madhav on 27-03-2017.
 */

public class GridTextureRegions {
    private ColoredRectangle[][] grid;
    private TextureReg[] trLast;
    private TextureReg[] trOriginal;
    private float width, height;
    private ArrayList<Integer> randomOrder;
    private ArrayList<Integer> originalOrder;
    private OnRegionClickListner onClickListner;
    private Vector2 vector2;
    private int rows, cols;

    public GridTextureRegions(int rows, int cols, float x, float y, float width, float height, ArrayList<Integer> originalOrder, ArrayList<Integer> randomOrder, OnRegionClickListner onClickListner) {
        vector2 = new Vector2(x, y);
        trLast = new TextureReg[rows * cols];
        trOriginal = new TextureReg[rows * cols];
        this.width = width;
        this.height = height;
        this.rows = rows;
        this.cols = cols;
        this.randomOrder = randomOrder;
        this.originalOrder = originalOrder;
        this.onClickListner = onClickListner;
    }

    public void init() {
        float rectWidth = width / cols;
        float rectHeight = height / rows;
        for (int i = 0; i < rows * cols; i++) {
            trOriginal[i] = new TextureReg(vector2.x + ((randomOrder.get(i) % cols) * rectWidth), vector2.y + (rows - (randomOrder.get(i) / rows) - 1) * rectHeight, vector2.x + ((i % cols) * rectWidth), vector2.y + (rows - (i / rows) - 1) * rectHeight, rectWidth - 2, rectHeight - 2, onClickListner, i);
trLast[i]= new TextureReg(vector2.x + ((i % cols) * rectWidth), vector2.y + (rows - (i / rows) - 1) * rectHeight, vector2.x + ((i % cols) * rectWidth), vector2.y + (rows - (i / rows) - 1) * rectHeight, rectWidth - 2, rectHeight - 2, onClickListner, i);
        }
    }


    public TextureReg[] getCurrentData() {
        return trOriginal;
    }

    public float getHeight() {
        return height;
    }

    public ArrayList<Integer> getRandomOrder() {
        return randomOrder;
    }

    public TextureReg[] changePosition(int blinkId, int changeId) {
        Vector2 pos1 = trOriginal[blinkId].getPosSrc();
        Vector2 pos2 = trOriginal[changeId].getPosSrc();
        trOriginal[changeId].setPosSrc(pos1);
        trOriginal[blinkId].setPosSrc(pos2);

System.out.println(isSame()?"complete":"not complete");
        return trOriginal;
    }
    private boolean isSame()
    {
        int i=0;
        while ( i<trOriginal.length) {
            if( !trOriginal[randomOrder.indexOf(i)].getPosBtn().equals(trLast[i].getPosBtn()))
            {
                return false;
            }
            i++;
        }
        return true;
    }
}
