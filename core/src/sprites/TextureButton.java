package sprites;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import interfaces.OnClickListener;

public class TextureButton {
    private Texture button;
    private Vector2 posBtn;
    private Rectangle boundsBtn;
    private float HEIGHT, WIDTH;
    private String id;
    private int idInt;
    private float multiplier = 0;
    private OnClickListener onClickListener;
    private float blinkMin, blinkMax, blinkSpeed, blinkComparator;
    private boolean isIncreasing = false;

    public TextureButton(float xPoint, float yPoint, float width, float height, String fileName, OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        HEIGHT = height;
        WIDTH = width;
        if (button != null) button.dispose();
        button = new Texture(fileName);
        posBtn = new Vector2(xPoint, yPoint);
        boundsBtn = new Rectangle(posBtn.x, posBtn.y, width, height);
        id = fileName.substring(0, fileName.indexOf(".png"));
    }

    public void isClick(Vector2 vector2) {
        if (boundsBtn.contains(vector2)) {
            if (onClickListener != null) {
                onClickListener.onClick(id);
            }
        }
    }

    public void dispose() {
        button.dispose();
    }


    public Texture getButton() {
        return button;
    }

    public Vector2 getPosBtn() {
        return posBtn;
    }

    public void setPosBtn(Vector2 posBtn) {
        this.posBtn = posBtn;
    }

    public Rectangle getBoundsBtn() {
        return boundsBtn;
    }

    public float getHEIGHT() {
        return HEIGHT;
    }

    public void setHEIGHT(float HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public float getWIDTH() {
        return WIDTH;
    }

    public void setWIDTH(float WIDTH) {
        this.WIDTH = WIDTH;
    }

    public void updateButton(String fileName) {
        button = new Texture(fileName);
        id = fileName.substring(0, fileName.indexOf(".png"));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIncreasing() {
        return isIncreasing;
    }

    public void setIncreasing(boolean increasing) {
        isIncreasing = increasing;
    }

    public float getBlinkMin() {
        return blinkMin;
    }

    public void setBlinkMin(float blinkMin) {
        this.blinkMin = blinkMin;
    }

    public float getBlinkMax() {
        return blinkMax;
    }

    public void setBlinkMax(float blinkMax) {
        this.blinkMax = blinkMax;
    }

    public float getBlinkSpeed() {
        return blinkSpeed;
    }

    public void setBlinkSpeed(float blinkSpeed) {
        this.blinkSpeed = blinkSpeed;
    }

    public float getBlinkComparator() {
        return blinkComparator;
    }

    public void setBlinkComparator(float blinkComparator) {
        this.blinkComparator = blinkComparator;
    }

    public void initBlink(float percentage, float speed) {
        blinkComparator = HEIGHT / WIDTH;
        blinkMax = WIDTH + (percentage / 100) * WIDTH;
        blinkMin = WIDTH;
        blinkSpeed = speed;
    }

    public void updateBlink() {
        if (isIncreasing) {
            if (getWIDTH() < blinkMax) {
                setWIDTH(WIDTH + blinkSpeed);
                setHEIGHT(HEIGHT + (blinkSpeed * blinkComparator));
                setPosBtn(new Vector2(posBtn.x - (blinkSpeed / 2), posBtn.y - (blinkSpeed * blinkComparator / 2)));
            } else {
                isIncreasing = false;
            }
        } else {
            if (getWIDTH() > blinkMin) {
                setWIDTH(WIDTH - blinkSpeed);
                setHEIGHT(HEIGHT - (blinkSpeed * blinkComparator));
                setPosBtn(new Vector2(posBtn.x + (blinkSpeed / 2), posBtn.y + (blinkSpeed * blinkComparator / 2)));
            } else {
                isIncreasing = true;
            }
        }
    }

    public void initOriginalPos() {
        setPosBtn(new Vector2(posBtn.x + (WIDTH - blinkMin) / 2, posBtn.y + (HEIGHT - (blinkMin * blinkComparator)) / 2));
        setWIDTH(blinkMin);
        setHEIGHT(blinkMin * blinkComparator);
    }

    public float getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(float maxWidth, float minWidth) {
        /*this.multiplier = Math.abs(minWidth - maxWidth) * 0.08f;*/
        multiplier = 1;
    }

    public void draw(SpriteBatch sb) {
        sb.draw(button, posBtn.x, posBtn.y, WIDTH, HEIGHT);
    }
}