package sprites;

import com.badlogic.gdx.graphics.Color;

import interfaces.OnClickListener;

/**
 * Created by madhav on 20-03-2017.
 */

public class GridView {
    private ColoredRectangle[][] grid;
    private TextureButton [][]textureButtons;
private float width,height;
    public GridView( int rows, int cols, float x, float y, float width, float height, OnClickListener onClickListener) {
        grid = new ColoredRectangle[rows][cols];
        this.width=width;
        this.height=height;
        float rectWidth = width / cols;
        float rectHeight = height / rows*1;
        for (int i = 0; i < rows; i++) {

            for (int j = 0; j < cols; j++) {
                grid[i][j] = new ColoredRectangle( x + (j * rectWidth), y + (( i) * rectHeight), rectWidth - 5, rectHeight - 5, Color.RED, onClickListener,String.valueOf(cols*i+j) );
            }
        }
    }

    public ColoredRectangle[][] getGrid() {
        return grid;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setGrid(ColoredRectangle[][] grid) {
        this.grid = grid;
    }
}
