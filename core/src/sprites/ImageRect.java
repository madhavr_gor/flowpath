package sprites;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import interfaces.OnClickListener;

/**
 * Created by madhav on 25-03-2017.
 */

public class ImageRect {
    private boolean projectionMatrixSet;
    private float width, height;
    private Vector2 pos;
    private Color color;
    private String id;
    private Rectangle boundsBtn;
    private OnClickListener onClickListener;
    public ImageRect(float x, float y, float width, float height, String filename, OnClickListener onClickListener, String id) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.id=id;
        this.onClickListener = onClickListener;
        projectionMatrixSet = false;
        pos = new Vector2(x, y);
        boundsBtn = new Rectangle(pos.x, pos.y, width, height);
    }

}
