package sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by madhav on 27-03-2017.
 */

public class TextureReg {
    private Vector2 posBtn;
    private Vector2 posSrc;
    private Rectangle boundsBtn;
    private float HEIGHT, WIDTH;
    private int id;
    private interfaces.OnRegionClickListner onClickListner;

    public TextureReg(float xPoint, float yPoint,float srcX,float srcY, float width, float height, interfaces.OnRegionClickListner onClickListner, int id) {
        this.onClickListner = onClickListner;
        HEIGHT = height;
        WIDTH = width;
        posBtn = new Vector2(xPoint, yPoint);
        posSrc=new Vector2(srcX,srcY);
        boundsBtn = new Rectangle(posBtn.x, posBtn.y, width, height);
        this.id = id;
        }

    public void isClick(Vector2 vector2) {
        if (boundsBtn.contains(vector2)) {
            if (onClickListner != null) {
                onClickListner.onClick(id);
            }
        }
    }

    public Vector2 getPosBtn() {
        return posBtn;
    }

    public Rectangle getBoundsBtn() {
        return boundsBtn;
    }

    public float getHEIGHT() {
        return HEIGHT;
    }

    public float getWIDTH() {
        return WIDTH;
    }

    public void setPosBtn(Vector2 posBtn) {
        this.posBtn = posBtn;
    }

    public void setHEIGHT(float HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public void setWIDTH(float WIDTH) {
        this.WIDTH = WIDTH;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vector2 getPosSrc() {
        return posSrc;
    }

    public void setPosSrc(Vector2 posSrc) {
        this.posSrc = posSrc;
    }
}
