package States;

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.utils.InputTransform;

import interfaces.OnClickListener;
import sprites.ColorRect;
import utils.MethodUtils;

import static com.badlogic.gdx.math.MathUtils.random;

/**
 * Created by madhav on 11-04-2017.
 */

public class BackGroundState extends State implements OnClickListener {
    private ColorRect backGround;
    private static final String STR_BG = "bg";
    private Array<ColorRect> rects;
    private float E_RECT=40;
    private int temp=0;

    public BackGroundState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT);
       // initRandoms();
        initTexture();
    }

    private void initTexture() {
        backGround = new ColorRect(cam, 0, 0, cam.viewportWidth, cam.viewportHeight, Color.BLACK, 0.8f);
        rects = new Array<ColorRect>();
        for (int i = 0; i < 20; i++) {
            rects.add(new ColorRect(cam, MethodUtils.rendomInt((int)cam.viewportWidth), MethodUtils.rendomInt((int)cam.viewportHeight), E_RECT, E_RECT, new Color(MethodUtils.rendomColorFloat(), MethodUtils.rendomColorFloat(), MethodUtils.rendomColorFloat(), 1), 1));
        }
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector2 v = InputTransform.getTouchVector();
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        for (ColorRect cr : rects) {

            cr.update();
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.end();
        for (ColorRect cr : rects) cr.draw();
        backGround.draw();
    }

    @Override
    public void dispose() {
        backGround.dispose();
        for (ColorRect colorRect : rects) {
            colorRect.dispose();
        }
    }

    @Override
    public void backPressed() {

    }


    @Override
    public void onClick(String id) {

    }
}
