package States;

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.utils.InputTransform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import interfaces.OnClickListener;
import interfaces.OnRegionClickListner;
import sprites.ColorRect;
import sprites.GridTextureRegions;
import sprites.TextureButton;
import sprites.TextureReg;

import static java.lang.System.*;

public class GameState extends State implements OnRegionClickListner, OnClickListener {
    private int rows = 4, cols = 4;
    private TextureButton tbImg;
    private GridTextureRegions gridView;
    private Texture texture;
    private TextureReg[] trOriginal;
    private TextureReg[] trTemp;
    private TextureReg trMoving;
    private TextureButton tbRestart;
    private Pixmap pixmap100, pixmap200;
    private boolean istouched;
    private int movingId;
    private String imgName;
    private ColorRect colorRect;


    public GameState(GameStateManager gsm, String imgName) {
        super(gsm);
        this.imgName = imgName;
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT);

        pixmap200 = new Pixmap(Gdx.files.internal(imgName));
        pixmap100 = new Pixmap((int) cam.viewportHeight, (int) cam.viewportHeight, pixmap200.getFormat());
        pixmap100.drawPixmap(pixmap200,
                0, 0, pixmap200.getWidth(), pixmap200.getHeight(),
                0, 0, pixmap100.getWidth(), pixmap100.getHeight()
        );
        texture = new Texture(pixmap100);
        init();
        initTextures();
        Gdx.input.setInputProcessor(new DrawingInput());
        Gdx.input.setCatchBackKey(true);
    }

    private void initTextures() {
        tbImg = new TextureButton(cam.viewportWidth - 280, 30, 250, 250, imgName, this);
        tbRestart = new TextureButton(cam.viewportWidth - 100, cam.viewportHeight - 100, 60, 60, "ic_restart.png", this);
    }

    private void init() {
        ArrayList<Integer> originalOrder = new
                ArrayList<Integer>();
        for (int i = 0; i < rows * cols; i++) {
            originalOrder.add(i);
        }

        ArrayList<Integer> randomOrder = randomNumberGenetration(originalOrder);
        gridView = new GridTextureRegions(rows, cols, 20, 20, cam.viewportHeight - 40, cam.viewportHeight - 40, originalOrder, randomOrder, this);
        gridView.init();
        trOriginal = gridView.getCurrentData();

    }

    private ArrayList<Integer> randomNumberGenetration(ArrayList<Integer> original) {
        ArrayList<Integer> abc = new ArrayList<Integer>();
        abc.addAll(original);
        Collections.shuffle(abc);
        out.println("original" + original);
        out.println("abc" + abc);
        return abc;
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector2 v = InputTransform.getTouchVector();
            for (TextureReg tr : trOriginal) {
                tr.isClick(v);
            }
            tbRestart.isClick(v);
        }
    }

    @Override

    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        tbImg.draw(sb);
        sb.draw(tbRestart.getButton(), tbRestart.getPosBtn().x, tbRestart.getPosBtn().y, tbRestart.getWIDTH(), tbRestart.getHEIGHT());
        for (int i = 0; i < trOriginal.length; i++) {
            TextureReg trOri = trOriginal[i];
            if (istouched) {
                if (i != movingId)
                    sb.draw(texture, trOri.getPosBtn().x, trOri.getPosBtn().y, trOri.getWIDTH(), trOri.getHEIGHT(), (int) trOri.getPosSrc().x, (int) trOri.getPosSrc().y, (int) trOri.getWIDTH(), (int) trOri.getHEIGHT(), false, false);
            } else {
                sb.draw(texture, trOri.getPosBtn().x, trOri.getPosBtn().y, trOri.getWIDTH(), trOri.getHEIGHT(), (int) trOri.getPosSrc().x, (int) trOri.getPosSrc().y, (int) trOri.getWIDTH(), (int) trOri.getHEIGHT(), false, false);
            }
        }
        if (istouched) {
            sb.draw(texture, trMoving.getPosBtn().x, trMoving.getPosBtn().y, trMoving.getWIDTH(), trMoving.getHEIGHT(), (int) trMoving.getPosSrc().x, (int) trMoving.getPosSrc().y, (int) trMoving.getWIDTH(), (int) trMoving.getHEIGHT(), false, false);
        }
        sb.end();
    }

    @Override
    public void dispose() {
        texture.dispose();
        pixmap200.dispose();
        pixmap100.dispose();
        tbImg.dispose();
        tbRestart.dispose();
    }

    @Override
    public void backPressed() {
        gsm.pop();
    }

    @Override
    public void onClick(int id) {

    }

    @Override
    public void onClick(String id) {
        if (id.equals(tbRestart.getId())) {
            init();
        }
    }

    private class DrawingInput extends InputAdapter {

        private Vector2 last = null;
        private boolean leftDown = false;

        private Vector2 diffMovingCentre;

        DrawingInput() {
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer,
                                 int button) {
            if (button == Input.Buttons.LEFT) {
                Vector2 start = getTouchVector(screenX, screenY);
                if (!istouched) {
                    for (TextureReg tr : trOriginal) {
                        if (tr.getBoundsBtn().contains(start)) {
                            istouched = true;
                            movingId = tr.getId();
                            diffMovingCentre = new Vector2(start.x - tr.getPosBtn().x, start.y - tr.getPosBtn().y);
                            trMoving = new TextureReg(tr.getPosBtn().x, tr.getPosBtn().y, tr.getPosSrc().x, tr.getPosSrc().y, tr.getWIDTH(), tr.getHEIGHT(), GameState.this, -1);
                            break;
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {

            if (istouched) {
                Vector2 curr = getTouchVector(screenX, screenY);
                trMoving.setPosBtn(curr.sub(diffMovingCentre));
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (button == Input.Buttons.LEFT) {
                if (istouched) {
                    Vector2 end = getTouchVector(screenX, screenY);
                    for (TextureReg tr : trOriginal) {
                        if (tr.getBoundsBtn().contains(end)) {
                            System.out.println(Arrays.toString(trOriginal));
                            gridView.changePosition(movingId, tr.getId());
                        }
                    }
                    istouched = false;
                }
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean keyDown(int keycode) {
            if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
                backPressed();
                return true;
            }
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        private Vector2 getTouchVector(int screenX, int screenY) {
            float X = InputTransform.getCursorToModelX();
            float Y = InputTransform.getCursorToModelY();
            return new Vector2(X, Y);
        }
    }
}
