package States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

import interfaces.AndroidConnectionInterface;

public class GameStateManager implements InputProcessor {
    private Stack<State> states;
    private AndroidConnectionInterface androidConnectionInterface;


    public GameStateManager(AndroidConnectionInterface androidConnectionInterface) {
        states = new Stack<State>();
        this.androidConnectionInterface = androidConnectionInterface;
        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
    }

    public void push(State state) {
        states.push(state);
    }

    public void pop() {
        states.pop().dispose();
        if (states.size() == 1) {
            states.pop().dispose();
            Gdx.app.exit();
            System.exit(0);
        }
    }

    public void set(State state) {
        states.pop().dispose();
        states.push(state);
    }

    public void update(float dt) {
        states.get(0).update(dt);
        states.peek().update(dt);
    }

    public void render(SpriteBatch sb) {
        states.get(0).render(sb);
        states.peek().render(sb);
    }

    public void dispose() {
        states.get(0).dispose();
        states.peek().dispose();
    }

    public AndroidConnectionInterface getAndroidConnectionInterface() {
        return androidConnectionInterface;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            states.peek().backPressed();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
