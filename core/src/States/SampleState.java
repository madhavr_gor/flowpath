package States;

/**
 * Created by Madhav Gor on 26-06-2017.
 */

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Madhav.Gor on 17/02/17.
 */

public class SampleState extends State {
    public SampleState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT );

    }

    @Override
    public void handleInput() {

    }


    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();

        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void backPressed() {
        gsm.pop();
    }


}
