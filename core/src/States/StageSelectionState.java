package States;

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.utils.InputTransform;

import interfaces.OnClickListener;
import sprites.TextFeild;
import sprites.TextureButton;
import utils.EnumUtils;
import utils.StringUtils;

/**
 * Created by Madhav Gor on 23-06-2017.
 */

public class StageSelectionState extends State implements OnClickListener {
    private TextureButton tbEasy;
    private TextureButton tbMedium;
    private TextureButton tbHard;

    public StageSelectionState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT);
        initTexture();
    }

    private void initTexture() {
        float mul = 0.5f;
        Texture t = new Texture(StringUtils.IMG_EASY);
        tbEasy = new TextureButton(((cam.viewportWidth / 4) - (t.getWidth() * mul) / 2), ((cam.viewportHeight * 3 / 4) - (t.getHeight() * mul) / 2), t.getWidth() * mul, t.getHeight() * mul, StringUtils.IMG_EASY, this);
        t = new Texture(StringUtils.IMG_MEDIUM);
        tbMedium = new TextureButton(((cam.viewportWidth * 3 / 5) - (t.getWidth() * mul) / 4), ((cam.viewportHeight * 3 / 4) - ((t.getHeight() * mul) / 2)), t.getWidth() * mul, t.getHeight() * mul, StringUtils.IMG_MEDIUM, this);
        t = new Texture(StringUtils.IMG_HARD);
        tbHard = new TextureButton(((cam.viewportWidth / 4) - (t.getWidth() * mul) / 2), ((cam.viewportHeight / 4) - (t.getHeight() * mul) / 2), t.getWidth() * mul, t.getHeight() * mul, StringUtils.IMG_HARD, this);
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector2 v = InputTransform.getTouchVector();
            tbEasy.isClick(v);
            tbMedium.isClick(v);
            tbHard.isClick(v);
        }
    }


    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        tbHard.draw(sb);
        tbMedium.draw(sb);
        tbEasy.draw(sb);
        sb.end();
    }

    @Override
    public void dispose() {
        tbEasy.dispose();
        tbMedium.dispose();
        tbHard.dispose();
    }

    @Override
    public void backPressed() {
        gsm.pop();
    }


    @Override
    public void onClick(String id) {
        if (id.equals(tbEasy.getId())) {
            gsm.push(new SelectGameState(gsm, EnumUtils.STAGE_TYPE.EASY));
        } else if (id.equals(tbHard.getId())) {
            gsm.push(new SelectGameState(gsm, EnumUtils.STAGE_TYPE.HARD));
        } else if (id.equals(tbMedium.getId())) {
            gsm.push(new SelectGameState(gsm, EnumUtils.STAGE_TYPE.MEDIUM));
        }
    }
}
