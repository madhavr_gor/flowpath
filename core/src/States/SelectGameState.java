package States;


import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.utils.InputTransform;

import custom.GridGameSelect;
import interfaces.OnClickListener;
import model.BinEasy;
import sprites.TextureButton;
import utils.DataParser;
import utils.EnumUtils;
import utils.StringUtils;

/**
 * Created by Madhav Gor on 22-06-2017.
 */

public class SelectGameState extends State implements OnClickListener {
    private EnumUtils.STAGE_TYPE stageType;
    private BinEasy binEasy;
    private GridGameSelect grid;
    private Array<TextureButton> tbs;
    private TextureButton tbBackward;
    private TextureButton tbForward;

    public SelectGameState(GameStateManager gsm, EnumUtils.STAGE_TYPE stageType) {
        super(gsm);
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT);
        this.stageType = stageType;
        binEasy = DataParser.getDataObject(BinEasy.class, StringUtils.GAME_ID_SELECT_GAME);
        initTexture();
        initGrid();
    }

    private void initTexture() {
        Texture t = new Texture(StringUtils.IMG_BACKWARD);
        tbBackward = new TextureButton(10, (cam.viewportHeight - t.getHeight()) / 2, t.getWidth(), t.getHeight(), StringUtils.IMG_BACKWARD, this);
        t = new Texture(StringUtils.IMG_BACKWARD);
        tbForward = new TextureButton(cam.viewportWidth - 10 - t.getWidth(), (cam.viewportHeight - t.getHeight()) / 2, t.getWidth(), t.getHeight(), StringUtils.IMG_FORWARD, this);
    }

    private void initGrid() {
        tbs = new Array<>();
        Array<String> arr = new Array<>();
        for (int i = 0; i < binEasy.getFile_names().size; i++) {
            arr.add(StringUtils.PACKAGE_ANIMALS + binEasy.getFile_names().get(i));
        }
        System.out.println(arr);
        grid = new GridGameSelect(50, 0, cam.viewportWidth - 100, cam.viewportHeight, 3, 4, tbs, arr, this);
        grid.setPaddingHor(15);
        grid.setPaddingVar(15);
        grid.init();
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector2 v = InputTransform.getTouchVector();
            for (TextureButton tb : tbs
                    ) {
                tb.isClick(v);
            }
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        tbForward.draw(sb);
        tbBackward.draw(sb);
        for (TextureButton textureButton : tbs) textureButton.draw(sb);
        sb.end();
    }

    @Override
    public void dispose() {
        for (TextureButton tb : tbs) tb.dispose();
        tbBackward.dispose();
        tbForward.dispose();
    }

    @Override
    public void backPressed() {
        gsm.pop();
    }

    @Override
    public void onClick(String id) {
        gsm.push(new GameState(gsm, id + ".png"));
    }
}
