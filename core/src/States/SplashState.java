package States;

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

import interfaces.OnClickListener;
import sprites.ColorRect;
import sprites.ColoredRectangle;
import sprites.TextureButton;
import utils.StringUtils;

/**
 * Created by Madhav Gor on 26-06-2017.
 */

public class SplashState extends State implements OnClickListener {
    private TextureButton tbCompanyName;
    private float mutliplier = 0.4f;
    private Array<ColorRect> coloredRectangles;
    private int counter = 0;
    private boolean isVertical;
private AnimState curAnimState;
    private float mul=4 ;

    public SplashState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        init();
        initRectangleAnimation();
        initTimer();
    }

    private void initTimer() {
        new Timer().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                gsm.set(new MenuState(gsm));
            }
        },5);
    }

    private void initRectangleAnimation() {
        // rectangle animation
        coloredRectangles = new Array<>();
        coloredRectangles.add(new  ColorRect(cam,360, 100, 20, 20, Color.WHITE,1));
        coloredRectangles.add(new ColorRect(cam,420, 160, 20, 20, Color.WHITE, 1));
        curAnimState=AnimState.UP;
    }

    private void init() {
        // company name
        Texture t = new Texture(StringUtils.IMG_COMPANY_NAME);
        tbCompanyName = new TextureButton((cam.viewportWidth - (t.getWidth() * mutliplier)) / 2, (cam.viewportHeight - (t.getHeight() * mutliplier)) / 1.5f, t.getWidth() * mutliplier, t.getHeight() * mutliplier, StringUtils.IMG_COMPANY_NAME, this);
    }

    @Override
    public void handleInput() {

    }


    @Override
    public void update(float dt) {
        handleInput();
        updateRects();
    }

    private void updateRects() {
        switch (curAnimState)
        {
            case UP:
                if (coloredRectangles.get(0).getPos().y < 160) {
                    coloredRectangles.get(0).setPos(new Vector2(coloredRectangles.get(0).getPos().x, coloredRectangles.get(0).getPos().y +mul));
                    coloredRectangles.get(1).setPos(new Vector2(coloredRectangles.get(1).getPos().x, coloredRectangles.get(1).getPos().y - mul));
                }
                else
                {
                    curAnimState=AnimState.RIGHT;
                }
                break;
            case RIGHT:
                if (coloredRectangles.get(0).getPos().x < 420) {
                    coloredRectangles.get(0).setPos(new Vector2(coloredRectangles.get(0).getPos().x+mul, coloredRectangles.get(0).getPos().y ));
                    coloredRectangles.get(1).setPos(new Vector2(coloredRectangles.get(1).getPos().x-mul, coloredRectangles.get(1).getPos().y ));
                }
                else
                {
                    curAnimState=AnimState.BOTTOM;
                }
                break;
            case BOTTOM:
                if (coloredRectangles.get(0).getPos().y >100) {
                    coloredRectangles.get(0).setPos(new Vector2(coloredRectangles.get(0).getPos().x, coloredRectangles.get(0).getPos().y - mul));
                    coloredRectangles.get(1).setPos(new Vector2(coloredRectangles.get(1).getPos().x, coloredRectangles.get(1).getPos().y + mul));
                }
                else
                {
                    curAnimState=AnimState.LEFT;
                }
                break;

            case LEFT:
                if (coloredRectangles.get(0).getPos().x >360) {
                    coloredRectangles.get(0).setPos(new Vector2(coloredRectangles.get(0).getPos().x-mul, coloredRectangles.get(0).getPos().y ));
                    coloredRectangles.get(1).setPos(new Vector2(coloredRectangles.get(1).getPos().x+mul, coloredRectangles.get(1).getPos().y ));
                }
                else
                {
                    curAnimState=AnimState.UP;
                }
                break;
        }

    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        tbCompanyName.draw(sb);
        sb.end();
        for (ColorRect cr:coloredRectangles
             ) {
            cr.draw();
        }
    }

    @Override
    public void dispose() {
        tbCompanyName.dispose();
        for (ColorRect cr:coloredRectangles
             ) {
            cr.dispose();
        }
    }

    @Override
    public void backPressed() {
        gsm.pop();
    }


    @Override
    public void onClick(String id) {

    }
    private enum AnimState{UP,LEFT,RIGHT,BOTTOM}
}
