package States;

import com.androidappers.flowpath.FlowPath;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.utils.InputTransform;

import interfaces.OnClickListener;

import sprites.TextFeild;
import sprites.TextureButton;
import utils.StringUtils;

/**
 * Created by madhav on 10-04-2017.
 */

public class MenuState extends State implements OnClickListener {
    private TextureButton tbPlay;

    public MenuState(GameStateManager gsm) {
        super(gsm);
        cam.setToOrtho(false, FlowPath.WIDTH, FlowPath.HEIGHT);
        initTexture();
    }

    private void initTexture() {

        Texture t = new Texture(StringUtils.IMG_PLAY);
        tbPlay = new TextureButton((cam.viewportWidth - t.getWidth()) / 2, (cam.viewportHeight - t.getHeight()) / 2, t.getWidth(), t.getHeight(), StringUtils.IMG_PLAY, this);

    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector2 v = InputTransform.getTouchVector();
      tbPlay.isClick(v);
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
   tbPlay.draw(sb);
        sb.end();
    }

    @Override
    public void dispose() {

    }

    @Override
    public void backPressed() {
        gsm.pop();
    }

    @Override
    public void onClick(String id) {
        if (id.equals(tbPlay.getId())) {
            gsm.push(new StageSelectionState(gsm));
        }
    }
}