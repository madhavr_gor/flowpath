package utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;

/**
 * Created by Madhav Gor on 22-06-2017.
 */

public class DataParser {

    public static void copyFile() {
        FileHandle file = Gdx.files.internal(StringUtils.JSON_GAME);
//        FileHandle fileLocal = Gdx.files.local(StringUtils.JSON_GAME2);
//        file.copyTo(fileLocal);
    }

    public static String getFileName(String key) {
        FileHandle file = Gdx.files.internal(StringUtils.JSON_GAME);
        String text = file.readString();
        JsonValue json = new JsonReader().parse(text);
        JsonValue gameJson = json.get(key);
        return gameJson.asString();
    }

    public static <T> T getDataObject(Class<T> type, String gameId, Integer... day) {
        System.out.println(type);
        FileHandle fileToRead = Gdx.files.internal(getFileName(gameId));
        String text2 = fileToRead.readString();
        JsonValue jsonMatecher = new JsonReader().parse(text2);
        JsonValue jsonObj = jsonMatecher.get((day != null && day.length > 0) ? String.valueOf(day[0]) : gameId);
        return new Json().fromJson(type, jsonObj.toJson(JsonWriter.OutputType.json));
    }

    public static String getString(String key, String jsonFileName) {
        FileHandle file = Gdx.files.internal(jsonFileName);
        String text = file.readString();
        JsonValue json = new JsonReader().parse(text);
        JsonValue gameJson = json.get(key);
        return gameJson.asString();
    }

    public static <T> Array<T> getDataArray(Class<T> type, String gameId, Integer... day) {
        Array<T> array = new Array<>();
        FileHandle fileToRead = Gdx.files.internal(getFileName(gameId));
        String text2 = fileToRead.readString();
        JsonValue jsonMatecher = new JsonReader().parse(text2);
        JsonValue jsonObj = jsonMatecher.get((day != null && day[0] != null) ? String.valueOf(day[0]) : gameId);
        jsonObj.setType(JsonValue.ValueType.array);
        for (int i = 0; i < jsonObj.size; i++) {
            JsonValue jv = jsonObj.get(i);
            T t = new Json().fromJson(type, jv.toJson(JsonWriter.OutputType.json));
            array.add(t);
        }
        return array;
    }
}