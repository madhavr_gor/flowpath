package utils;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by madhav on 20-03-2017.
 */
public class ShapeRenderSingleton extends ShapeRenderer {

    private static ShapeRenderSingleton ourInstance = new ShapeRenderSingleton();

    public static ShapeRenderSingleton getInstance() {
        return ourInstance;
    }

    private ShapeRenderSingleton() {
    }

}
