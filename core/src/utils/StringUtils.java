package utils;

/**
 * Created by madhav on 10-04-2017.
 */

public class StringUtils {
    //  ###FONT NAME###
    public static final String FONT_FILE_NAME = "fonts/text.ttf";
    public static final String FONT_ROBOTO_BOLD = "fonts/roboto_bold.ttf";
    public static final String FONT_ROBOTO_THIN = "fonts/roboto_thin.ttf";
    public static final String FONT_ROBOTO_REGULAR = "fonts/roboto_regular.ttf";

    //  ###JSON FILE NAME###
    public static final String JSON_GAME = "json/game.json";

    //  ###game id###
    public static final String GAME_ID_SELECT_GAME = "easy";

    //  ###package names###
    public static final String PACKAGE_ANIMALS = "animals/";

    // ###IMAGE names###
    public static final String IMG_COMPANY_NAME = "company_name.png";
    public static final String IMG_BACKWARD = "other/backward.png";
    public static final String IMG_FORWARD = "other/forward.png";
    public static final String IMG_EASY = "img_stage_names/easy.png";
    public static final String IMG_HARD = "img_stage_names/hard.png";
    public static final String IMG_MEDIUM = "img_stage_names/medium.png";
    public static final String IMG_PLAY = "other/play.png";
    //text color B0FFFF
}
