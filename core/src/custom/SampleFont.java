package custom;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import utils.StringUtils;

public class SampleFont {
    private static SampleFont myObj;
    private BitmapFont bitmapFont;

    public SampleFont() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(StringUtils.FONT_ROBOTO_BOLD));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 40;
        parameter.color = Color.SKY;
        bitmapFont = generator.generateFont(parameter);
        generator.dispose();
    }

    public static SampleFont getInstance() {
        if (myObj == null) {
            myObj = new SampleFont();
        }
        return myObj;
    }

    public BitmapFont getBitmapFont() {
        return bitmapFont;
    }

    public void setBitmapFont(BitmapFont bitmapFont) {
        this.bitmapFont = bitmapFont;
    }
}