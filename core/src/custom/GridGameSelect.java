package custom;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;


import interfaces.OnClickListener;
import sprites.TextureButton;

/**
 * Created by Madhav Gor on 24-06-2017.
 */

public class GridGameSelect {
    private float width, height;
    private Vector2 pos;
    private Array<TextureButton> tbs;
    private Array<String> tbNames;
    private int tbCount;
    private int cols;
    private int rows;
    private OnClickListener onClickListener;
    private float paddingHor;
    private float paddingVar;

    public GridGameSelect(float x, float y, float width, float height, int rows, int cols, Array<TextureButton> tbs, Array<String> tbNames, OnClickListener onClickListener) {
        pos = new Vector2(x, y);
        this.width = width;
        this.height = height;
        this.tbs = tbs;
        this.tbNames = tbNames;
        this.tbCount = tbNames.size;
        this.rows = rows;
        this.cols = cols;
        this.onClickListener = onClickListener;
    }

    public void init() {
        float tbWidth = (width - (paddingVar * (cols + 1))) / cols;
        float tbHeight = (height - (paddingVar * (rows + 1))) / rows;
        if (tbs == null) tbs = new Array<>();
        else tbs.clear();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (((i * rows) + j) < tbCount) {
                    tbs.add(new TextureButton(pos.x + (j * tbWidth)+((j+1)*paddingVar), pos.y + (((rows - i - 1) * tbHeight))+((rows-i)*paddingHor), tbWidth, tbHeight, tbNames.get((i * rows) + j), onClickListener));
                }
            }
        }
    }

    public void dispose() {
        if (tbs != null) for (TextureButton tb : tbs) tb.dispose();
    }

    public void draw(SpriteBatch sb) {
    }

    public void setPaddingHor(float paddingHor) {
        this.paddingHor = paddingHor;
    }

    public void setPaddingVar(float paddingVar) {
        this.paddingVar = paddingVar;
    }
}
