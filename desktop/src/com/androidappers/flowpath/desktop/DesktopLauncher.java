package com.androidappers.flowpath.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.androidappers.flowpath.FlowPath;

import interfaces.AndroidConnectionInterface;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.height = FlowPath.HEIGHT;
        config.width = FlowPath.WIDTH;
        new LwjglApplication(new FlowPath(new AndroidConnectionInterface() {
            @Override
            public void disToast(String s) {

            }

            @Override
            public void picImageFromFile() {

            }

            @Override
            public void startNewActivity() {

            }
        }), config);
    }

}
